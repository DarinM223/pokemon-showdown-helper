module Main where

import Prelude
import Effect (Effect)
import Effect.Console (log)
import JQuery (JQuery, JQueryEvent, on, select)

foreign import onEnterPokemonShowdown :: Effect Unit -> Effect Unit
foreign import alert :: String -> Effect Unit

doPrint :: Effect Unit
doPrint = log "Hello chrome!"

-- TODO(DarinM223): activate when icon is clicked, then display a drop down with
-- possible moves

main :: Effect Unit
main = onEnterPokemonShowdown $ do
  alert "Hello!"
  let selector = ".battle-log > .inner"
  chatbox <- select selector
  on "DOMSubtreeModified" (handleChange chatbox) chatbox
 where
  handleChange :: JQuery -> JQueryEvent -> JQuery -> Effect Unit
  handleChange chatbox event _ = do
    log "Change detected!"
    pure unit
