'use strict';

exports.onEnterPokemonShowdown = function(f) {
  return function() {
    chrome.webNavigation.onCompleted.addListener(function() {
      f();
    }, {url: [{urlMatches: 'https://play.pokemonshowdown.com'}]});
  }
};

exports.alert = function(s) {
  return function() {
    alert(s);
  }
};
